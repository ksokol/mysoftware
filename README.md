# mysoftware

Just a list of software I use on daily basis

## Core

Ubuntu 20.04.4 LTS 

## Teminal

k9s
kubectx
kubens

## Kafka

kowl
kafdrop

## Beautifiers

Starship
Delta

# Gnome extensions
dash to panel
openweather
user themes
vitals
